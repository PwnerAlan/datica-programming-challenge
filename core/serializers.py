from rest_framework import serializers
from .models import UserAccount

# Setting up serializers for the db models/ fields
class UserAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccount
        fields = '__all__'