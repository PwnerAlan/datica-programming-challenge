from django import forms


class UserSignUp(forms.Form):
    username = forms.CharField(max_length=400)
    email = forms.EmailField(max_length=400)
    password = forms.CharField(max_length=999)


class UserSignIn(forms.Form):
    username = forms.CharField(max_length=400)
    password = forms.CharField(max_length=999)
