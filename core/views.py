# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from .models import UserAccount
from .forms import UserSignUp, UserSignIn
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import UserAccountSerializer
from django.http import JsonResponse

# Create your views here.


class UserView(APIView):

    # initially get user account data if it exists
    def get_user(self, username):
        try:
            return UserAccount.objects.get(username=username)
        except UserAccount.DoesNotExist:
            raise Http404

    # Grab and save user data if valid, render userprofile if data valid
    def post(self, request, format=None):
        serializer = UserAccountSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            request.session['username'] = serializer.validated_data['username']
        return render(request, 'core/userprofile.html', context={'username': request.session['username']})

    # if session or username is invalid force user to signup or view profile if not
    def get(self, request, username=None, format=None):
        if request.session['username'] is None or username is None:
            form = UserSignUp()
            return render(request, 'core/signup.html', context={'signupform': form})
        else:
            user = self.get_user(username)
            serializer = UserAccountSerializer(user)
            return render(request, 'core/userprofile.html', context={'username': serializer.data['username']})

    # Validation check for serializer
    def put(self, request, username, format=None):
        user = self.get_user(username)
        serializer = UserAccountSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return render(request, 'core')
        return render(request, status=status.HTTP_400_BAD_REQUEST)

    # Delete user option
    def delete(self, request, username, format=None):
        user = self.get_user(username)
        user.delete()
        request.session['username'] = None
        return JsonResponse({'url': 'core/user_deleted.html'})

# Incorporate the serializers when dealing with the session data and requests
class UserAuthView(APIView):

    # sign in page/ form for user to get their session info and sign in to the page
    # if user session invalid redirects to initial sign up page
    def get(self, request, format=None):
        if request.session['username'] is None:
            form = UserSignIn()
            return render(request, 'core/signin.html', context={'signinform': form})
        else:
            return redirect('/user/%s' % (request.session['username']))

    # if user sign in is valid, gather data and show profile page (delete account / see json data)
    # TODO: Add other json options
    def post(self, request, format=None):
        user_sign = UserAccountSerializer(data=request.data)
        if user_sign.is_valid():
            user = UserAccount.objects.get(username=user_sign.cleaned_data['username'])

            if user.password == user_sign.cleaned_data['password']:
                request.session['username'] = user.username
                return render(request, 'core/userprofile.html', context={'username': user.username})

    # utilized JavaScript/ Ajax instead of deleting profile here
    def delete(self, request, format=None):
        pass

# index page
def index(request):
    return render(request, 'core/index.html')



# first attempt at initial setup
# def signup(request):
#     form = UserSignUp()
#     if request.method == 'POST':
#         # pass the post data to a form object to check integrity
#         user_signup = UserSignUp(request.POST)
#         # validate data
#         if user_signup.is_valid():
#             # create new user from validated data
#             UserAccount.objects.create(username=user_signup.cleaned_data['username'], email=user_signup.cleaned_data['email'], password=user_signup.cleaned_data['password'])
#     return render(request, 'core/signup.html', context={'signupform': form})
#
#
# def user_action(request):
#
#     form = UserSignIn()
#     if request.method == 'GET':
#         if request.session['username'] is None:
#             pass
#         else:
#             return render(request, 'core/userprofile.html', context={'username': request.session['username']})
#
#     elif request.method == 'POST':
#         user_sign = UserSignIn(request.POST)
#
#         if user_sign.is_valid():
#             user = UserAccount.objects.get(username=user_sign.cleaned_data['username'])
#
#             if user.password == user_sign.cleaned_data['password']:
#                 request.session['username'] = user.username
#                 return render(request, 'core/userprofile.html', context={'username': user.username})
#
#     return render(request, 'core/signin.html', context={'signinform': form})
