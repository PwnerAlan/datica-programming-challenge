from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.index),
    url(r'^user/$', views.UserView.as_view()),
    url(r'^user/(?P<username>[-\w]+)/$', views.UserView.as_view()),
    url(r'^auth/$', views.UserAuthView.as_view()),

]
