# README #

Thank you very much for this awesome experience and opportunity to show you what I'm capable under a short time frame. 
I wish I had some more time to work on it! I'm looking forward to talking with you further.

### What is this repository for? ###

* Datica Programming Challenge

### How do I get set up? ###

* Summary of set up & Deployment instructions

	1) Initially set up project and open terminal
	2) $: python manage.py runserver
	3) Open Browser and go to address "127.0.0.1:8000"
	4) Click Sign Up
	5) Input Username, Email, and Password
	6) Brought to user screen displaying username & Delete account option
	7) If account deleted, brought back to sign up page
	8) Admin Acess: 
		- More visible JSON Data and Administrative functions utilizing Django's frameworks
		- Access to User mMdification

### Who do I talk to? ###

If you need me to give you or more people access to this repository please feel free to call or email me at (262) 720-2039 or aeckerx@gmail.com

